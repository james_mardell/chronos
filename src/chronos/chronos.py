#!/usr/bin/env python
# -*- coding: utf-8 -*-


from PIL import Image, ImageTk, ImageDraw
from math import sin, cos, radians
from datetime import datetime
from pytz import timezone
from Tkinter import Label, Tk


class SimpleImageViewer(object):
  def __init__(self, animator, width=64, height=32, *args, **kwargs):
    self.animator = animator
    self.width = width
    self.height = height
    self.scale_factor = 2

    self.root = Tk()
    self.root.bind('<Button>', self.exit_main_loop)
    self.root.geometry('+%d+%d' % (0, 0))  # top-left
    self.root.geometry('%dx%d' % (width, height))
    self.root.title(u'SimpleImageViewer')

  def exit_main_loop(self, event):
    event.widget.quit()

  def animate(self):
    raw_image = self.animator()
    image = raw_image.resize((self.width, self.height), Image.BICUBIC)
    photo = ImageTk.PhotoImage(image)
    label = Label(self.root, image=photo)
    label.place(x=0, y=0, width=image.size[0], height=image.size[1])
    self.root.after(20, self.animate)
    self.root.mainloop()


class Chronos(object):
  """
  Much risk. Such diligence. Many profit.
  """
  width = 64
  height = 32

  def __init__(self, *args, **kwargs):
    self.image = Image.new('RGB', (self.width * 2, self.height * 2), 'black')
    self.draw = ImageDraw.Draw(self.image)

  def run(self):
    siv = SimpleImageViewer(animator=self.clock)
    siv.animate()

  def _draw_clock_hand(self, center, angle, length, thickness, colour='#ffffff'):
    cx, cy = center
    x0 = cx + length * cos(radians(angle - 90.0))
    y0 = cy + length * sin(radians(angle - 90.0))
    self.draw.line((cx, cy, x0, y0), fill=colour, width=thickness)

  def _get_time_angles(self, tz='US/Eastern'):
    now = datetime.now(timezone(tz))

    h = (now.hour - 1.0) / 12.0
    m = now.minute / 60.0
    s = now.second / 60.0
    us = now.microsecond / 1000000.0

    hour_angle = (h + (m / 12.0)) * 360.0
    minute_angle = (m + (s / 60.0)) * 360.0
    second_angle = (s + (us / 60.0)) * 360.0

    return hour_angle, minute_angle, second_angle

  def _draw_clock(self, center=(32, 32), tz='US/Eastern'):
    h, m, s = self._get_time_angles(tz)
    self._draw_clock_hand(center, s, 24, 2, colour='#1b75bc')
    self._draw_clock_hand(center, m, 24, 4)
    self._draw_clock_hand(center, h, 16, 6)

  def _draw_background(self):
    self.draw.rectangle((0, 0, self.width * 2, self.height * 2), fill='#00aeef')
    self.draw.ellipse((4, 4, 60, 60), fill='#000000')
    self.draw.ellipse((68, 4, 124, 60), fill='#000000')

  def clock(self):
    self._draw_background()
    self._draw_clock((32, 32), tz='Europe/London')
    self._draw_clock((96, 32), tz='US/Eastern')
    return self.image
