from setuptools import setup, find_packages

with open('requirements.txt') as requirements_file:
  requirements = list(requirements_file.readlines())

setup(
  name = 'chronos',
  version = '0.0.1',
  packages = find_packages('src'),
  package_dir = {'': 'src'},
  install_requires = requirements,
)
