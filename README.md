# Chronos

## Description

The personification of time, or a world clock for output on a AdaFruit RGB LED matrix.

## Developing

Invoke the packaging, linting, testing and test coverage rountines by running `tox`. Note that common library issues can be fixed by regenerating the package using `tox -r`.

The tests can be run manually using `./bin/test.sh`.

The scraper can be invoked manually by using `./bin/run.sh`.
