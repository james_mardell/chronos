#!/usr/bin/env python
# -*- coding: utf-8 -*-


import sys
from chronos import Chronos


if (__name__ == "__main__"):
  chronos_instance = Chronos(*sys.argv)
  chronos_instance.run()
